<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */

/* @var $searchModel app\models\Events */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Админ-панель';
?>

<?= Html::a('Создать мероприятие', ['event-save?id='], ['class' => 'btn btn-success']) ?>

<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'name',
                'format'    => 'html',
                'value'     => function ($searchModel) {
                    return Html::a(Html::encode($searchModel->name), ['event-save', 'id' => $searchModel->id]);
                },
            ],
            [
                'attribute' => 'dt_show',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'organizer_names',
                'format'    => 'raw',
                'value'     => function ($searchModel) {
                    return $searchModel->getOrganizerNames();
                },
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'urlCreator' => function ($action, $model) {
                    return 'admin/event-delete?id=' . $model->id;
                }
            ],
        ]
    ]);
    ?>
</div>

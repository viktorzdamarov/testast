<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 *  * This is the model class for table "organizers".
 *
 * @property integer $id
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property Events[] $events
 */

class Organizers extends ActiveRecord
{
    public $event_names;

    public function rules()
    {
        return [
            [['id', 'fio', 'email', 'phone', 'event_names'], 'default', 'value'=> ''],
            [['fio', 'email'], 'required']
        ];
    }

    public function search($params)
    {

        $query = $this->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->id))
            $query->andWhere(['id' => $this->id]);

        if (!empty($this->fio))
            $query->andWhere('UPPER(fio) LIKE UPPER("%' . $this->fio . '%")');

        if (!empty($this->event_names))
            $query->joinWith('events')
                ->andWhere('UPPER(name) LIKE UPPER("%' . $this->event_names . '%")');


        $dataProvider->sort->attributes['id'] = [
            'asc' => ['id' => SORT_ASC],
            'desc' => ['id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['fio'] = [
            'asc' => ['fio' => SORT_ASC],
            'desc' => ['fio' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['email' => SORT_ASC],
            'desc' => ['email' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['created_at' => SORT_ASC],
            'desc' => ['created_at' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['updated_at'] = [
            'asc' => ['updated_at' => SORT_ASC],
            'desc' => ['updated_at' => SORT_DESC],
        ];

        return $dataProvider;
    }

    /** @inheritdoc */
    public function getEvents()
    {
        return $this->hasMany(Events::class, ['id' => 'event_id'])->viaTable('events_organizers', ['organizer_id' => 'id']);
    }

    public function getEventNames()
    {
        $ev = [];
        foreach ($this->events as $events) {
            $ev[] = $events->name;
        }
        return implode(', ', $ev);
    }

    public function getEventsIds()
    {
        $ev= [];
        foreach ($this->events as $events) {
            $ev[] = $events->id;
        }
        return implode(', ', $ev);
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'fio' => 'Организатор',
            'email' => 'E-Mail',
            'phone' => 'Телефон',
            'event_names' => 'Мероприятия',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    public function eventsData()
    {
        return ArrayHelper::map(Events::find()->all(), 'id', 'name');
    }

    public function saveEventsOrganizers($data)
    {
        EventsOrganizers::deleteAll(['organizer_id' => $this->id]);

        if (empty($data)) return false;
        foreach ($data as $event_id) {
            $row = new EventsOrganizers();
            $row->event_id = $event_id;
            $row->organizer_id = $this->id;
            $row->save();
        }
    }
}
<?php


namespace app\controllers;

use app\models\Events;
use app\models\Organizers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class AdminController extends Controller
{
    public $layout = 'admin';

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new Events();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionOrganizers()
    {
        $searchModel = new Organizers();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('organizers', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionEventDelete($id)
    {
        $event = Events::findOne($id);
        $event->delete();

        Yii::$app->response->redirect(Url::to('/admin'));
    }

    public function actionOrganizerDelete($id)
    {
        $organizer = Organizers::findOne($id);
        $organizer->delete();

        Yii::$app->response->redirect(Url::to('/admin/organizers'));
    }

    public function actionEventSave($id)
    {
        $post = \Yii::$app->request->post();

        $event = empty($id) ? new Events() : Events::findOne($id);

        if (($event) and ($event->load($post))) {
            if ($event->save()) {
                $event->saveEventsOrganizers($post['Events']['organizer_names']);
                return $this->actionIndex();
            }
        }

        return $this->render('event-save', [
            'model' => $event
        ]);

    }

    public function actionOrganizerSave($id)
    {
        $post = \Yii::$app->request->post();

        $organizer = empty($id) ? new Organizers() : Organizers::findOne($id);

        if (($organizer) and ($organizer->load($post))) {
            if ($organizer->save()) {
                $organizer->saveEventsOrganizers($post['Organizers']['event_names']);
                return $this->actionOrganizers();
            }
        }

        return $this->render('organizer-save', [
            'model' => $organizer
        ]);

    }
}
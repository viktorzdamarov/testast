<?php

use yii\grid\GridView;

/** @var yii\web\View $this */

/* @var $searchModel app\models\Events */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мероприятия';
?>
<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'name',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'dt_show',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'organizer_names',
                'format'    => 'raw',
                'value'     => function ($searchModel) {
                    return $searchModel->getOrganizerNames();
                },
            ],
            [
                'attribute' => 'description',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'raw',
            ],
        ]
    ]);
    ?>
</div>

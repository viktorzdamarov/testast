<?php

use yii\db\Migration;

/**
 * Class m240529_165220_first
 */
class m240529_165220_first extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        // Мероприятия
        $this->createTable('events', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(150)->notNull(),
            'dt_show'       => $this->date()->null(),
            'description'   => $this->string()->null(),
            'created_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        // Организаторы
        $this->createTable('organizers', [
            'id'            => $this->primaryKey(),
            'fio'           => $this->string(150)->notNull(),
            'email'         => $this->string(150)->notNull(),
            'phone'         => $this->string(20)->null(),
            'created_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'    => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        //Таблица связей
        $this->createTable('events_organizers', [
            'event_id'      => $this->integer()->notNull(),
            'organizer_id'  => $this->integer()->notNull(),
            'PRIMARY KEY (event_id, organizer_id)',
        ]);

        // Устанавливаем индексы для ускорения запросов
        $this->createIndex('idx-events_organizers-event_id', 'events_organizers', 'event_id');
        $this->createIndex('idx-events_organizers-organizer_id', 'events_organizers', 'organizer_id');

        // Устанавливаем foreign key для таблицы events_organizers
        $this->addForeignKey('fk-events_organizers-event_id', 'events_organizers', 'event_id', 'events', 'id', 'CASCADE');
        $this->addForeignKey('fk-events_organizers-organizer_id', 'events_organizers', 'organizer_id', 'organizers', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {

        // Удаляем foreign key
        $this->dropForeignKey('fk-events_organizers-event_id', 'events_organizers');
        $this->dropForeignKey('idx-events_organizers-organizer_id', 'events_organizers');

        // Удаляем индексы
        $this->dropIndex('idx-events_organizers-event_id', 'events_organizers');
        $this->dropIndex('idx-events_organizers-organizer_id', 'events_organizers');

        // Удаляем таблицы
        $this->dropTable('events_organizers');
        $this->dropTable('events');
        $this->dropTable('organizers');

    }
}

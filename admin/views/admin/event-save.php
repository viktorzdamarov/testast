<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/**
  * @var $model app\models\Events
*/

if ($model->isNewRecord) {
    $this->title = 'Создание мероприятия:';
} else {
    $this->title = 'Редактирование мероприятия: ' . $model->name;
}

$orgValue = !empty($model->getOrganizerIds()) ? explode(',', $model->getOrganizerIds()) : null;

?>
<h1><?= $this->title ?></h1>

<div class="event-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'dt_show')->widget(DatePicker::class, [
        'convertFormat' => true,
        'removeButton' => false,
        'pluginOptions' => [
            'format' => 'php:Y-m-d',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'organizer_names')->widget(Select2::classname(), [
        'data' => $model->organizersData(),
        'options' => [
            'multiple' => true,
            'value' => $orgValue,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Отмена', '/admin', ['class' => 'btn btn-link']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

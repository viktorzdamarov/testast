<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/**
  * @var $model app\models\Events
*/

if ($model->isNewRecord) {
    $this->title = 'Создание организатора:';
} else {
    $this->title = 'Редактирование организатора: ' . $model->fio;
}

?>
<h1><?= $this->title ?></h1>

<div class="event-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'fio') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'phone') ?>

    <?= $form->field($model, 'event_names')->widget(Select2::classname(), [
        'data' => $model->eventsData(),
        'options' => [
            'multiple' => true,
            'value' => explode(',', $model->getEventsIds()),
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Отмена', '/admin/organizers', ['class' => 'btn btn-link']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

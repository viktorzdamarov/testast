<?php

use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */

/* @var $searchModel app\models\Events */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Админ-панель';
?>

<?= Html::a('Создать организатора', ['organizer-save?id='], ['class' => 'btn btn-success']) ?>

<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'fio',
                'format'    => 'html',
                'value'     => function ($searchModel) {
                    return Html::a(Html::encode($searchModel->fio), ['organizer-save', 'id' => $searchModel->id]);
                },
            ],
            [
                'attribute' => 'email',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'phone',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'event_names',
                'format'    => 'raw',
                'value'     => function ($searchModel) {
                    return $searchModel->getEventNames();
                },
            ],
            [
                'attribute' => 'created_at',
                'format'    => 'raw',
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'urlCreator' => function ($action, $model) {
                    return 'organizer-delete?id=' . $model->id;
                }
            ],
        ]
    ]);
    ?>
</div>

<?php


namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 *  * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $name
 * @property string $dt_show
 * @property string $description
 * @property Organizers[] $organizers
 */

class Events extends ActiveRecord
{
    public $organizer_names = null;

    public function rules()
    {
        return [
            [['id','name', 'dt_show', 'description', 'organizer_names'], 'default', 'value'=> ''],
            [['name'], 'required']
        ];
    }

    public function search($params)
    {

        $query = $this->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->id))
            $query->andWhere(['id' => $this->id]);

        if (!empty($this->name))
            $query->andWhere('UPPER(name) LIKE UPPER("%' . $this->name . '%")');

        if (!empty($this->organizer_names))
            $query->joinWith('organizers')
                ->andWhere('UPPER(fio) LIKE UPPER("%' . $this->organizer_names . '%")');


        $dataProvider->sort->attributes['id'] = [
            'asc' => ['id' => SORT_ASC],
            'desc' => ['id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['name'] = [
            'asc' => ['name' => SORT_ASC],
            'desc' => ['name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['dt_show'] = [
            'asc' => ['dt_show' => SORT_ASC],
            'desc' => ['dt_show' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['created_at' => SORT_ASC],
            'desc' => ['created_at' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['updated_at'] = [
            'asc' => ['updated_at' => SORT_ASC],
            'desc' => ['updated_at' => SORT_DESC],
        ];

        return $dataProvider;
    }

    /** @inheritdoc */
    public function getOrganizers()
    {
        return $this->hasMany(Organizers::class, ['id' => 'organizer_id'])->viaTable('events_organizers', ['event_id' => 'id']);
    }

    public function getOrganizerNames()
    {
        $orgs = [];
        foreach ($this->organizers as $org) {
            $orgs[] = $org->fio;
        }
        return implode(', ', $orgs);
    }

    public function getOrganizerIds()
    {
        $orgs = [];
        foreach ($this->organizers as $org) {
            $orgs[] = $org->id;
        }
        return implode(', ', $orgs);
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Название',
            'dt_show' => 'Дата проведения',
            'description' => 'Описание',
            'organizer_names' => 'Организаторы',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    public function organizersData()
    {
        return ArrayHelper::map(Organizers::find()->all(), 'id', 'fio');
    }

    public function saveEventsOrganizers($data)
    {
        EventsOrganizers::deleteAll(['event_id' => $this->id]);

        if (empty($data)) return false;
        foreach ($data as $orgId) {
            $row = new EventsOrganizers();
            $row->event_id = $this->id;
            $row->organizer_id = $orgId;
            $row->save();
        }
    }
}